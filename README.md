# OpenML dataset: child_0

https://www.openml.org/d/45131

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Child Bayesian Network. Sample 0.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-medium.html#child)

- Number of nodes: 20

- Number of arcs: 25

- Number of parameters: 230

- Average Markov blanket size: 3

- Average degree: 2.5

- Maximum in-degree: 2

**Authors**: David J. Spiegelhalter, A. Philip Dawid, Steffen L. Lauritzen and Robert G. Cowell

**Please cite**: ([URL](https://cse.sc.edu/~mgv/csce582sp21/links/SDLC_CHILD_1993.pdf)): D. J. Spiegelhalter, R. G. Cowell (1992). Learning in probabilistic expert systems. In Bayesian Statistics 4 (J. M. Bernardo, J. 0. Berger, A. P. Dawid and A. F. M. Smith, eds.), 447-466. Clarendon Press, Oxford.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45131) of an [OpenML dataset](https://www.openml.org/d/45131). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45131/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45131/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45131/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

